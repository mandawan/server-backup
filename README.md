### Server-backup
This script enable automatic backup of directory using Rsync and OVH's backup storage.
His goal is to be launched by a cronjob every night, for auto backup of the modified directories.

The script execute the list of commands described under this description.
To add a new directory to backup, you'll have to add a new line to the commands dictionary.
The key will be the path of the new directory to save and the value the Rsync command to execute.
Backup directory lives in /home/backup, when you had a new dir to save, create the corresponding directory in /home/backup respecting the following architecture :
* every configuration files lives in ```/home/backup/conf/name-of-the-service```
* every new websites lives in ```/home/backup/sites/name-of-my-website```
* every logs lives in ```/home/backup/logs/logs-name```

An example command will be : ```rsync -aEim --delete path/to/original/directory/to/save /path/of/backup/directory```
