#!/usr/bin/python
import subprocess

# =============================================================================

# This script enable automatic backup of directory using Rsync and OVH's backup storage
# His goal is to be launched by a cronjob every night, for auto backup of the modified directories

# The script execute the list of commands described under this description
# To add a new directory to backup, you'll have to add a new line to the commands dictionnary
# The key will be the path of the new directory to save and the value the Rsync
# command to execute.
# Backup directory lives in /home/backup, when you had a new dir to save, create the
# corresponding directory in /home/backup respecting the following architecture :
# every configuration files lives in /home/backup/conf/name-of-the-service
# every new websites lives in /home/backup/sites/name-of-my-website
# every logs lives in /home/backup/logs/logs-name
# an example command will be : rsync -aEim --delete path/to/original/directory/to/save /path/of/backup/directory

# =============================================================================

commands = {
"/home/user/dir" : "rsync -aEim --exclude '/dir/to/exclude/' --delete path/to/original/directory/to/save /path/of/backup/directory",
"/var/www/my-website" : "rsync -aEim --delete /var/www/my-website /home/backup/my-website/"
}

new_dirs_to_backup = []


for path in commands:
    print("Executing cmd : "+commands[path])
    # Execute commands
    cmd = subprocess.Popen(commands[path], shell=True, stdout=subprocess.PIPE)
    # wait till cmd is executed before continue
    cmd.wait()
    # Get cmd output
    cmd_output = cmd.communicate()[0].decode("utf-8")
    cmd_output.rstrip('\n')
    print("CMD OUTPUT : "+cmd_output)
    if cmd_output == "":
        print("no changes to rsync")
    else:
        print("rsync did change")
        new_dirs_to_backup.append(path)

# If we have new directory to archive
if new_dirs_to_backup != []:
    date_cmd = subprocess.Popen("echo $(date +%F)", shell=True, stdout=subprocess.PIPE)
    date_cmd.wait()
    today_date = date_cmd.communicate()[0].decode("utf-8")
    # Remove unvanted line return
    date_line = today_date.splitlines()
    today_date = ''.join(date_line)
    # mkdir the daily archive folder
    cmd1 = subprocess.Popen("mkdir /home/backup/backup-"+today_date, shell=True, stdout=subprocess.PIPE)
    cmd1.wait()
    for new_dir in new_dirs_to_backup:
        # copy folders with changes in daily backup
        print("cp -r "+new_dir+" /home/backup/backup-"+today_date)
        cmd2 = subprocess.Popen("cp -r "+new_dir+" /home/backup/backup-"+today_date, shell=True, stdout=subprocess.PIPE)
        cmd2.wait()
    # zip daily archive folder
    print("ziping => /home/backup/backup-"+today_date+".zip")
    cmd3 = subprocess.Popen("zip -r /home/backup/backup-"+today_date+".zip /home/backup/backup-"+today_date, shell=True, stdout=subprocess.PIPE)
    cmd3_output = cmd3.communicate()[0].decode("utf-8")
    print(cmd3_output)
    cmd3.wait()
    # send it to the backup server
    cmd4 = subprocess.Popen("lftp ftp://my-ftp-user:my-ft-password@my-ftp-server:21 -e \'cd /; put /home/backup/backup-"+today_date+".zip; quit\'", shell=True, stdout=subprocess.PIPE)
    cmd4_output = cmd4.communicate()[0].decode("utf-8")
    if "bytes transferred" in cmd4_output:
        print("Transfer was successful, deleting created archive")
        cmd5 = subprocess.Popen("rm /home/backup/backup-"+today_date+".zip", shell=True, stdout=subprocess.PIPE)
        cmd5.wait()
        cmd6 = subprocess.Popen("rm -r /home/backup/backup-"+today_date, shell=True, stdout=subprocess.PIPE)
        cmd6.wait()
    else:
        print("Problem transfering datas with lftp")
else:
    print("no new directory to backup")
